<?php
require 'conexion.php';

//INICIO CONSULTA COMUNAS
	$consultaComunas = $db->prepare("SELECT *  FROM comunas");
	$consultaComunas -> execute();

	$arregloComunas=array();
	$contComunas=0;

	while($consult=$consultaComunas->fetch(PDO::FETCH_OBJ))
	{
		$arregloComunas[$contComunas]['id']=$consult->id;
		$arregloComunas[$contComunas]['comuna']=$consult->comuna;
		$contComunas++;
	}
//FIN CONSULTA COMUNAS

//INICIO CONSULTA REGION
	$consultaRegion = $db->prepare("SELECT * FROM region");
	$consultaRegion -> execute();
	$arregloRegion=array();
	$contRegion=0;

	while($consult=$consultaRegion->fetch(PDO::FETCH_OBJ))
	{
		$arregloRegion[$contRegion]['id']=$consult->id;
		$arregloRegion[$contRegion]['region']=$consult->region;
		$contRegion++;
	}
//FIN CONSULTA REGION

//INICIO CONSULTA PERSONAS
	$consultaPersonas = $db->prepare("SELECT * FROM personas");
	$consultaPersonas->execute();
	$arregloPersonas=array();
	$contPersonas=0;

	while($consult=$consultaPersonas->fetch(PDO::FETCH_OBJ))
	{
		$arregloPersonas[$contPersonas]['id']=$consult->id;
		$arregloPersonas[$contPersonas]['nombres']=$consult->nombres;
		$arregloPersonas[$contPersonas]['apellido_paterno']=$consult->apellido_paterno;
		$arregloPersonas[$contPersonas]['apellido_materno']=$consult->apellido_materno;
		$arregloPersonas[$contPersonas]['fecha_nacimineto']=$consult->fecha_nacimiento;
		$arregloPersonas[$contPersonas]['sexo']=$consult->sexo;
		$arregloPersonas[$contPersonas]['rut_sin_digito']=$consult->rut_sin_digito;
		$arregloPersonas[$contPersonas]['digito_verificador']=$consult->digito_verificador;
		$arregloPersonas[$contPersonas]['domicilio']=$consult->domicilio;
		$arregloPersonas[$contPersonas]['numero']=$consult->numero;
		$arregloPersonas[$contPersonas]['telefono']=$consult->telefono;
		$arregloPersonas[$contPersonas]['fecha_ingreso']=$consult->fecha_ingreso;
		$contPersonas++;
	}
//FIN CONSULTA PERSONAS

//INICIO ACCIONES
	//INICIO ACCION GUARDAR
	if(isset($_POST['ingresar']))
	{
		//INSERT COMUNAS
		$insertComunas = $db->prepare("INSERT INTO comunas (comuna) VALUES (?)");
		$insertComunas -> execute(array($_POST['nombreC']));

		//INSERT REGION
		$inserRegion = $db->prepare("INSERT INTO region (region) VALUES (?)");
		$inserRegion-> execute(array($_POST['nombreR']));

		//INSERT PERSONAS
		$insertPersonas = $db->prepare("INSERT INTO personas (nombres,apellido_paterno,apellido_materno,fecha_nacimiento,
		sexo,rut_sin_digito,digito_verificador,domicilio,numero,telefono,fecha_ingreso) VALUES (?,?,?,?,?,?,?,?,?,?,now()");
		$insertPersonas->execute(array($_POST['nombreP']));

		header('location: ingreso.php');
	}
	//FIN ACCION GUARDAR
//FIN ACCIONES

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Ingreso</title>
	<link href="estilos-fabian.css" rel="stylesheet" type="text/css">
</head>
<body>
	<form action="<?php $PHP_SELF; ?>" method="POST">
		<div align="center">
			<div class="efectoNeon">PRUEBA</div>
			<table border="1">

				<!--INICIO INGRESO COMUNA -->
					<td><strong>Nombre comuna</strong></td>
					<td><input required="required" type="text" name="nombreC" placeholder="Ingrese comuna"></td>
					<td>Listado Comunas</td>
					<td><select name="listaComunas">
					<option value="">Seleccione</option>
<?php 				foreach($arregloComunas as $arreComunas)
					{?>
						<option value="<?php echo $arreComunas['id'] ?>"><?php echo $arreComunas['comuna'] ?></option>
<?php  				}?>
				<!--FIN INGRESO COMUNA -->

				<!-- INICIO INGRESO REGION -->
                <tr>
					<td><strong>Nombre region</strong></td>
					<td><input requiered="requiered" type="text" name="nombreR" placeholder="Ingrese region"></td>
					<td>Listado Region</td>
					<td><select name="listaRegion">
					<option value="">Seleccione</option>
<?php 				foreach($arregloRegion as $arreRegion)
					{?>
						<option value="<?php echo $arreRegion['id'] ?>"><?php echo $arreRegion['region']?></option>
<?php 				}?>
					</select></td>
                </tr>
				<!-- FIN INGRESO REGION -->

                <!-- INICIO INGRESO PERSONAS -->
                <tr>
                    <td><strong>Ficha Postulación</strong></td>
                    <td><input requiered="requiered" type="text" name="nombres" placeholder="Ingrese nombres"></td>
                    <td><input requiered="requiered" type="text" name="apellido_paterno" placeholder="Ingrese apellido paterno"></td>
                    <td><input requiered="requiered" type="text" name="apellido_materno" placeholder="Ingrese apellido materno"></td>
                    <td><input requiered="requiered" type="text" name="fecha_nacimiento" placeholder="0000-00-00"></td>
                    <td><input requiered="requiered" type="text" name="sexo" placeholder="F o M "></td>
                    <td><input requiered="requiered" type="text" name="rut_sin_digito" placeholder="00000000"></td>
                    <td><input requiered="requiered" type="text" name="digito_verificador" placeholder="0"></td>
                    <td><input requiered="requiered" type="text" name="domicilio" placeholder="Ingrese domicilio"></td>
                    <td><input requiered="requiered" type="text" name="numero" placeholder="Ingrese numero"></td>
                    <td><input requiered="requiered" type="text" name="telefono" placeholder="Ingrese telefono"></td>
                    <td><input requiered="requiered" type="text" name="fecha_ingreso" placeholder="0000-00-00"></td>
                </tr>

			</table>
			<div align="center"><input type="submit" class="aprobar" name="ingresar" value="Ingresar" style="width: 130px"></div>
		</div>
	</form>
</body>
</html>